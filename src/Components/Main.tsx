import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Example from "../Pages/Example";
import Index from "../Pages/Index";

export default class Main extends Component {
  render() {
    return (
      <main>
        <Switch>
          <Route path="/example" component={Example} />
          <Route path="/" component={Index} />
        </Switch>
      </main>
    );
  }
}
