import React, { Component } from "react";
import { Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

export default class Navbar extends Component {
  render() {
    return (
      <React.Fragment>
        <Menu as="nav">
          <Link to="/">
            <Menu.Item header>SIP App</Menu.Item>
          </Link>
          <Link to="/example">
            <Menu.Item name="example" />
          </Link>
        </Menu>
      </React.Fragment>
    );
  }
}
