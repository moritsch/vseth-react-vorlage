import React, { Component } from "react";
import { Container } from "semantic-ui-react";

export default class Example extends Component {
  render() {
    return (
      <Container>
        <h1>Example</h1>
      </Container>
    );
  }
}
