import React, { Component } from "react";
import "semantic-ui-css/semantic.min.css";
import Navbar from "./Components/Navbar";
import { BrowserRouter } from "react-router-dom";
import Main from "./Components/Main";
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Navbar />
          <Main />
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
